import argparse


stopCodons=set({"TAG","TGA","TAA"})
allCodons=set({'AGC', 'CAT', 'TCG', 'GAA', 'TGG', 'TGA', 'GAT', 'CCC', 'CTT', 'CGG', 'GCT', 'TGT', 'AGA', 'TAA', 'GTA', 'CTC', 'AAC', 'CAC', 'AAA', 'CGA', 'TGC', 'TTA', 'TTG', 'ATC', 'GAG', 'TCT', 'GCA', 'GTC', 'AAT', 'GGA', 'CCA', 'TTT', 'TCA', 'GGT', 'TAT', 'TAC', 'GAC', 'GGC', 'AAG', 'CTG', 'GCG', 'AGG', 'TAG', 'GCC', 'CGT', 'ACA', 'TCC', 'TTC', 'ATG', 'CGC', 'ATA', 'GTG', 'CTA', 'GTT', 'GGG', 'ACC', 'CCG', 'AGT', 'CAA', 'CAG', 'ATT', 'CCT', 'ACT', 'ACG'})

def wczytajPlik(plik):
    slownik={}
    for linia in plik:
        linia=linia.strip()
        if linia=="":
            continue
        if linia[0]==">":
            aktualnaSekwencja=linia[1:]
            slownik[aktualnaSekwencja]=""
        else:
            slownik[aktualnaSekwencja]=slownik[aktualnaSekwencja]+linia
    return slownik


def czyDlugoscPodzielnaPrzez3(slownik):
    for klucz in slownik:
        if len(slownik[klucz])%3==0:
            continue
        else:
            slownik[klucz]=""
    return slownik


def czyWSrodkuKodonStop(slownik):
    for klucz in slownik:
        if slownik[klucz][-3:] not in stopCodons:
            slownik[klucz]=""
            continue
        liczbaKodonowStop=0
        for i in range(len(slownik[klucz])-3, -1, -3):
            if slownik[klucz][i:i+3] in stopCodons:
                liczbaKodonowStop+=1
            else:
                break

  #      slownik[klucz]
        for i in range(0,len(slownik[klucz])-liczbaKodonowStop*3,3):
            if slownik[klucz][i:i+3] in stopCodons:
                slownik[klucz]=""
                break
    return slownik


def usunZNiepoprawnymiNukleotydami(slownik):
    for klucz in slownik:
        for i in range(0, len(slownik[klucz]), 3):
            if slownik[klucz][i:i+3] not in allCodons:
                slownik[klucz]=""
                break
    return slownik


def usunPuste(slownik):
    kluczeDoUsuniecia=[]
    for klucz in slownik:
        if slownik[klucz]=="":
            kluczeDoUsuniecia.append(klucz)
    for klucz in kluczeDoUsuniecia:
        del slownik[klucz]
    return slownik

def calosc(sciezka):
    plikWejsciowy=open(sciezka,"r")
    plikWyjsciowy=open(sciezka+".filtred","w")
    slownik=wczytajPlik(plikWejsciowy)
    slownik=czyDlugoscPodzielnaPrzez3(slownik)
    slownik=usunZNiepoprawnymiNukleotydami(slownik)
    slownik=czyWSrodkuKodonStop(slownik)
    slownik=usunPuste(slownik)
    for klucz in slownik:
        print(">",klucz,sep="", file=plikWyjsciowy)
        print(slownik[klucz],file=plikWyjsciowy)
    return


obiektParser=argparse.ArgumentParser()
obiektParser.add_argument("file", nargs="+",
                          help="Paths to fasta files.")

argumenty=obiektParser.parse_args()

for sciezka in argumenty.file:
    calosc(sciezka)

