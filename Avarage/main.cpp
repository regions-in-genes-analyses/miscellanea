#include <iostream>
#include <fstream>
#include <sstream>
#include<vector>
#include <boost/filesystem.hpp>

int main(int argc, char** argv)
{
  //In _c_compose is 18 fields about nucleotide fraction
  //orf length
  //4 fields with text
  //128 fields about codons fractions
  //192 fields about codon fractions in regions
  if(argc==1)
  {
    std::cerr<<"Second argument should be directory with compose files.\n";
    exit(1);
  }
  std::ofstream outFile;
  outFile.open("mean_c_composes.txt");
  boost::filesystem::directory_iterator inDir(argv[1]);
  std::string header;
  bool ifHeaderPrinted=0;
  for(;inDir!=boost::filesystem::directory_iterator();inDir++)
  {
    std::vector<double> sum;
    sum.resize(147+192,0);
    int counter=0;
    std::ifstream inFile;
    inFile.open(inDir->path().generic_string());
    std::string line;
    getline(inFile, header);
    while(!inFile.eof())
    {
      getline(inFile,line);
      if(line.empty())
      {
        break;
      }
      counter++;
      std::stringstream lineStream(line);
      std::string field;
      getline(lineStream,field, '\t');
      for(int i=0;i<19;i++)
      {
        getline(lineStream,field, '\t');
        sum[i]+=std::stod(field);
      }
      //skip start codon, stop codon, atypical number, error message
      getline(lineStream,field, '\t');
      getline(lineStream,field, '\t');
      getline(lineStream,field, '\t');
      getline(lineStream,field, '\t');
      for(int i=0;i<320;i++)
      {
        getline(lineStream,field, '\t');
        sum[i+19]+=std::stod(field);
      }
    }
    if(!ifHeaderPrinted)
    {
      outFile<<header<<"\n";
      ifHeaderPrinted=1;
    }
    outFile<<inDir->path().filename()<<"\t";
    for(int i=0;i<19;i++)
    {
      outFile<<sum[i]/counter<<"\t";
    }
    outFile<<"\t\t\t\t";
    for(int i=0;i<320;i++)
    {
      outFile<<sum[i+19]/counter<<"\t";
    }
    outFile<<"\n";
    inFile.close();
  }
  outFile.close();
  return 0;

}
