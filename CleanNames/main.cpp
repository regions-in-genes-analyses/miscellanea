#include <iostream>
#include <fstream>
#include <vector>

void printHelp()
{
  std::cout<<"This function take one argument. Path to namesfile.\n";
}

int main(int argc, char** argv)
{
  if(argc!=2)
  {
    printHelp();
    exit(0);
  }

  std::ifstream fileToClean;
  std::ofstream cleanedFile;

  fileToClean.open(argv[1]);
  cleanedFile.open(std::string(argv[1])+"_clean");

  bool ifSpace=0;

  std::string line;
  while(!fileToClean.eof())
  {
    ifSpace=0;
    std::string clean="";
    getline(fileToClean,line);
    
    for(unsigned int itInLine=0;itInLine<line.size();itInLine++)
    {
      if(!isspace(line[itInLine]) && line[itInLine]!='\n' && iscntrl(line[itInLine]))
        ///is nor space and is contron
      {
        continue;
      }
      if(line[itInLine]==' ' || line[itInLine]=='\t')
      {
        ifSpace=1;
        continue;
      }
      if(ifSpace==1)
      {
        clean+=' ';
        ifSpace=0;
      }
      clean+=line[itInLine];
      continue;
    }
    cleanedFile<<clean<<"\n";
  }
  return 0;
}
