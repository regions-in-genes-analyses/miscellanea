#!/bin/bash

#1 number of groups
#2 number of genoms
#3 nuber of regions
#4 out folder
#5 in names file
#6 in gnuplot.csv file

gnuplotScript=`dirname "$(realpath $0)"`/HisCG.gpi;

if test ! -e $4
then
  mkdir -p $4
fi

exec 6<$6;

for ((i=0;i<$2;i++))
do
  for((j=0;j<$1;j++))
  do

    read -u 6 title group;

    exec 5<$5;
    while read -u 5 fileName name
    do
      if [ "$fileName" = "$title" ]
      then 
        title=$name
      fi
    done;
    exec 5<&-;

    gnuplot -c "$gnuplotScript" $1 $j $i "$title" "$6"  >"$4/$title"_"$group.png";

    for((k=0;k<$3+2;k++))
    do
      read -u 6;
    done;

    echo $title $group;
  done

done;
