#!/bin/bash

#1 number of groups
#2 number of genoms
#3 nuber of regions
#4 out folder
#5 in names file
#6 in gnuplot.csv file

gnuplotScript=`dirname "$(realpath $0)"`/His4.gpi;
#echo $gnuplotScript;

if test ! -e $4
then
  mkdir -p $4
fi

exec 6<$6;

for ((i=1;i<=$2;i++))
do
  read -u 6 title rest;
  
  exec 5<$5;
  while read -u 5 fileName name
  do
    if [ "$fileName" = "$title" ]
    then 
      title=$name
    fi
  done;
  exec 5<&-;

  gnuplot -c $gnuplotScript $1 $i "$title" "$6" >"$4/$title.png";

  for ((j=1;j<$1*($3+1)+($1*2);j++))
  do
    read -u 6 rest;
  done;
done;
