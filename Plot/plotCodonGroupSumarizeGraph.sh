#!/bin/bash

#1 number of groups
#2 number of genoms
#3 number of regions
#4 out folder
#5 in name file (unused)
#6 in gnuplot.csv file

gnuplotScript=`dirname "$(realpath $0)"`/HisR.gpi;

if test ! -e $4
then
  mkdir -p $4
fi

exec 6<$6;

for ((i=1;i<=$1;i++))
do
  read -u 6 title group;
  
#  exec 5<$5;
#  while read -u 5 fileName name
#  do
#    if [ "$fileName" = "$title" ]
#    then 
#      title=$name
#    fi
#  done;
#  exec 5<&-;

  gnuplot -c "$gnuplotScript" $1 $2 $i "$group" "$6" >"$4/$group.png";

  for ((j=1;j<2+$3+1;j++))
  do
    read -u 6 rest;
  done;
done;
